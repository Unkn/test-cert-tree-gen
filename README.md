# Test Cert Tree Generator

Generates a tree of certificates given json input describing the tree. This
project is only intended for easily generating certificates for testing
TLS connection validation based on properties of the chain of trust for the
client and server.

```
git clone git@gitlab.com:Unkn/test-cert-tree-gen.git
cd test-cert-tree-gen
mkdir build
cd build
# -DOPENSSL_ROOT_DIR=$HOME/git/openssl/
cmake ../.
make
./test-cert-tree-gen -f ../example.tree
c_rehash .
```
