/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "arg_parser.h"

class tree_gen_arg_parser : public unkn::arg_parser
{
    public:
        enum : int
        {
            INPUT_FILE = 0
        };

        tree_gen_arg_parser()
        {
            add_opt< unkn::string_arg_opt >(INPUT_FILE, 'f', "file",
                    "JSON formatted file describing the tree to generate");
        }

        virtual ~tree_gen_arg_parser() {}
};
