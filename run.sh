#!/bin/sh -x

OPTIND=1 # Reset
OPENSSL=openssl
APP=s_server
DIR=
KEY=
CERT=
ARGS="-Verify 1 -unlink"
CARGS= #"-verify_return_error -verify 1"

is_server()
{
    [ $APP = "s_server" ]
}

while getopts "gcd:" opt; do
    case "$opt" in

        c)  APP=s_client
            ARGS=$CARGS
            ;;

        d)
            DIR="$OPTARG"
            KEY=$(find "$DIR" -name \*.key.pem)
            if [ -z "$KEY" ]; then
                >&2 echo "Failed to find key file"
                exit 1
            fi
            CERT=$(echo $KEY | sed 's/\.key\.pem$/.cert.pem/')
            ;;

        g)
            # OpenSSL build from master to use tls 1.3
            OPENSSL_REPO=$HOME/git/openssl
            OPENSSL=$OPENSSL_REPO/apps/openssl

            # Need to tell ld where to find the ssl and crypto libs
            export LD_LIBRARY_PATH="$(realpath $OPENSSL_REPO)"
            fuck=$(ldd $OPENSSL | grep "not found")
            if [ ! -z "$fuck" ]; then
                >&2 echo Failed to find openssl libraries
                exit 1
            fi
            ;;

    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

if [ ! -d "$DIR" ]; then
    >&2 echo "Invalid certificate directory"
fi

cgdb --args $OPENSSL $APP $ARGS -tls1_3 -CApath "$DIR" -verifyCApath "$DIR" -chainCApath "$DIR" -cert "$CERT" -key "$KEY" -unix fuck $@
#$OPENSSL $APP $ARGS -state -verify_return_error -tls1_3 -CApath "$DIR" -cert "$CERT" -key "$KEY" -unix fuck $@
