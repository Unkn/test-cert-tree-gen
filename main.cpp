/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <functional>
#include <openssl/objects.h>
#include <openssl/pem.h>
#include <openssl/x509v3.h>
#include <openssl/ssl.h>
#include <list>
#include <memory>
#include <sstream>

#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>

// http://nion.modprobe.de/blog/archives/357-Recursive-directory-creation.html
static void _mkdir(const char *dir)
{
    char tmp[256];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp), "%s", dir);
    len = strlen(tmp);
    if (tmp[len - 1] == '/')
    {
        tmp[len - 1] = 0;
    }
    for (p = tmp + 1; *p; p++)
    if (*p == '/')
    {
        *p = 0;
        mkdir(tmp, S_IRWXU);
        *p = '/';
    }
    mkdir(tmp, S_IRWXU);
}

#include "tao/json.hpp"
#include "Logger.h"
#include "StdOutLogMethod.h"

#include "tree_gen_arg_parser.h"
#include "cert_node_factory.h"

using children = std::vector< tao::json::value >;

struct state
{
    std::unique_ptr< cert_node > cert_;
    const children * children_;
    children::size_type current_child_;


    state(
            std::unique_ptr< cert_node > && cert,
            const children * children,
            children::size_type current_child) :
        cert_(std::move(cert)),
        children_(children),
        current_child_(current_child)
    {
    }

    state() = delete;
    state(const state &) = delete;
    state(state &&) = default;
    ~state() {}
};


static bool cert_tree_walk(const tao::json::value & v, cert_node_factory & f);
static const tao::json::value * get_from_object(const tao::json::value & v, const std::string & id);
static bool get_cert_from_object(const tao::json::value & v, const tao::json::value *& c);
static bool get_children_from_object(const tao::json::value & v, const children *& c);
static bool generate_key_and_cert(const cert_node & issuer, cert_node & new_cert, int serial);
static auto new_key(const cert_node & cn);
static bool add_ext(X509 * issuer, X509 * subject, int nid, const char * value);
static bool generate_key_and_cert(const cert_node & issuer, cert_node & new_cert, int serial);
static bool save_cert_node(const cert_node & cn, const std::string & dir, bool save_key);
static bool save_cert_chain(const std::list< state > & chain);

static tree_gen_arg_parser parsed_args;

int main(int argc, char **argv)
{
    OPENSSL_init();
    OpenSSL_add_all_algorithms();
    OpenSSL_add_ssl_algorithms();

    USE_STDOUT_LOG_METHOD;

    if (!parsed_args.parse(argc, argv))
        return -1;

    auto input_file = parsed_args.get_opt(tree_gen_arg_parser::INPUT_FILE)
        .value<const std::string &>();

    tao::json::value v;
    try
    {
        v = tao::json::parse_file(input_file);
    }
    catch (...)
    {
        LOG_ERROR("Failed to parse file {}", input_file);
        return -1;
    }

    cert_node d;
    d.country_ = "US";
    d.state_ = "CA";
    d.locality_ = "Glamis";
    d.org_ = "Unkn";
    d.org_unit_ = "Unkn MS";
    d.curve_nid_ = NID_secp521r1;
    cert_node_factory f(d);
    if (!cert_tree_walk(v, f))
    {
        LOG_ERROR("Failed to process cert tree");
        return -1;
    }

    EVP_cleanup();

    return 0;
}

bool cert_tree_walk(const tao::json::value & v, cert_node_factory & f)
{
    std::list< state > stack;

    std::unique_ptr< cert_node > cn;
    const tao::json::value * cv = nullptr;
    const children * c = nullptr;

    if (!get_cert_from_object(v, cv))
    {
        LOG_ERROR("Failed to retrieve root cert object");
        return false;
    }

    cn = f.create(*cv);
    if (!cn)
    {
        LOG_ERROR("Failed to process root cert object");
        return false;
    }

    if (!get_children_from_object(v, c))
    {
        LOG_ERROR("Failed to retrieve children of root cert object");
        return false;
    }

    if (!generate_key_and_cert(*cn, *cn, 0))
    {
        LOG_ERROR("Failed to generate X509 cert for root cert object");
        return false;
    }

    stack.emplace_back(std::move(cn), c, 0);

    auto get_index_path = [&stack]()
    {
        bool first = true;
        std::stringstream ss;
        ss << "[";
        for (auto & s : stack)
        {
            if (first)
                first = false;
            else
                ss << ", ";
            ss << s.current_child_;
        }
        ss << "]";
        return ss.str();
    };

    while (!stack.empty())
    {
        auto & s = stack.back();
        if (s.children_ && s.current_child_ < s.children_->size())
        {
            auto & cur_val = s.children_->at(s.current_child_);

            if (!get_cert_from_object(cur_val, cv))
            {
                LOG_ERROR("Failed to retrieve child cert object {}", get_index_path());
                return false;
            }

            cn = f.create(*cv);
            if (!cn)
            {
                LOG_ERROR("Failed to process child cert object {}", get_index_path());
                return false;
            }

            if (!get_children_from_object(cur_val, c))
            {
                LOG_ERROR("Failed to retrieve children of child cert object {}", get_index_path());
                return false;
            }

            if (!cn->ca_ && c)
            {
                LOG_ERROR("Non-CA cert object {} has child certs", get_index_path());
                return false;
            }

            if (!generate_key_and_cert(*s.cert_, *cn, s.current_child_))
            {
                LOG_ERROR("Failed to generate X509 cert for child cert object {}", get_index_path());
                return false;
            }


            stack.emplace_back(std::move(cn), c, 0);

            if (!stack.back().cert_->ca_ && !save_cert_chain(stack)/*node(*cn)*/)
            {
                LOG_ERROR("Failed to write child cert object {} to file system", get_index_path());
                return false;
            }

            ++s.current_child_;

            continue;
        }

        stack.pop_back();
    }

    return true;
}

const tao::json::value * get_from_object(const tao::json::value & v, const std::string & id)
{
    using namespace tao::json;

    if (v.type() != type::OBJECT)
    {
        LOG_ERROR("Expected value type {} but got {}",
                json_type_to_str(type::OBJECT),
                json_type_to_str(v.type()));
        return nullptr;
    }

    auto & obj = v.unsafe_get_object();
    auto it = obj.find(id);
    if (it == obj.end())
    {
        LOG_ERROR("Failed to find '{}' in object", id);
        return nullptr;
    }

    return &it->second;
}

bool get_cert_from_object(const tao::json::value & v, const tao::json::value *& c)
{
    c = get_from_object(v, "cert");
    return c != nullptr;
}

bool get_children_from_object(const tao::json::value & v, const children *& c)
{
    using namespace tao::json;

    c = nullptr;
    auto tmp = get_from_object(v, "children");
    if (!tmp)
    {
        return true;
    }

    if (tmp->type() != type::ARRAY)
    {
        LOG_ERROR("Expected value type {} but got {}",
                json_type_to_str(type::ARRAY),
                json_type_to_str(tmp->type()));
        return false;
    }

    c = &tmp->unsafe_get_array();
    return true;
}

auto new_key(const cert_node & cn)
{
    auto ret = std::unique_ptr< EVP_PKEY, decltype(&EVP_PKEY_free) >(nullptr, &EVP_PKEY_free);
    auto k = std::unique_ptr< EC_KEY, decltype(&EC_KEY_free) >(
            EC_KEY_new_by_curve_name(cn.curve_nid_), &EC_KEY_free);

    if (!k)
    {
        LOG_ERROR("Failed to initialize key with NID {}", cn.curve_nid_);
        return ret;
    }

    if (!EC_KEY_generate_key(k.get()))
    {
        LOG_ERROR("Failed to generate key");
        return ret;
    }

    EC_KEY_set_asn1_flag(k.get(), OPENSSL_EC_NAMED_CURVE);
    ret.reset(EVP_PKEY_new());
    if (!EVP_PKEY_assign_EC_KEY(ret.get(), k.get()))
    {
        LOG_ERROR("Failed to assign key to evp pkey");
        ret.reset();
        return ret;
    }
    k.release();
    return ret;
}

bool add_ext(X509 * issuer, X509 * subject, int nid, const char * value)
{
    X509V3_CTX ctx;
    X509V3_set_ctx_nodb(&ctx);
    X509V3_set_ctx(&ctx, issuer, subject, nullptr, nullptr, 0);
    X509_EXTENSION * ex = X509V3_EXT_conf_nid(nullptr, &ctx, nid, (char *)value);
    if (!ex)
        return false;
    X509_add_ext(subject, ex, -1);
    X509_EXTENSION_free(ex);
    return true;
}

bool generate_key_and_cert(const cert_node & issuer, cert_node & new_cert, int serial)
{
    LOG_INFO("Generating new cert/key pair with issuer '{}' and subject '{}'",
            issuer.common_name_, new_cert.common_name_);

    auto key = new_key(new_cert);
    if (!key)
        return false;

    auto x509 = std::unique_ptr< X509, decltype(&X509_free) >(X509_new(), &X509_free);
    if (!x509)
    {
        LOG_ERROR("Failed to allocate new X509 object");
        return false;
    }

    auto x = x509.get();
    bool self_signed = &issuer == &new_cert;

    // Set serial number
    ASN1_INTEGER_set(X509_get_serialNumber(x), serial);
    // Set not before to now
    X509_gmtime_adj(X509_get_notBefore(x), 0);
    // Set not after to 356 days from now in seconds
    X509_gmtime_adj(X509_get_notAfter(x), 31536000L);
    // Set cert public key
    if (!X509_set_pubkey(x, key.get()))
    {
        LOG_ERROR("Failed to set public key for cert");
        return false;
    }

    issuer.set_name(X509_get_issuer_name(x));
    new_cert.set_name(X509_get_subject_name(x));

    if (new_cert.ca_)
    {
        auto i = self_signed ? x : issuer.x509_.get();
        add_ext(i, x, NID_basic_constraints, "critical,CA:TRUE");
        add_ext(i, x, NID_key_usage, "critical,keyCertSign,cRLSign");
    }

    if (!X509_sign(x, self_signed ? key.get() : issuer.key_.get(), EVP_sha256()))
    {
        LOG_ERROR("Failed to sign new cert");
        return false;
    }

    new_cert.x509_ = std::move(x509);
    new_cert.key_ = std::move(key);

    return true;
}

void replace(std::string & s, char find, char rep)
{
    std::replace(s.begin(), s.end(), find, rep);
}

bool save_cert_node(const cert_node & cn, const std::string & dir, bool save_key)
{
    std::string partial = cn.common_name_;
    replace(partial, ' ', '_');
    partial.insert(0, dir);

    std::string sk = partial + ".key.pem";
    std::string sc = partial + ".cert.pem";

    if (save_key)
    {
        FILE * fk = fopen(sk.data(), "wb");
        if (!fk)
        {
            LOG_ERROR("Failed to open key file '{}' for writing", sk);
            return false;
        }
        PEM_write_PrivateKey(fk, cn.key_.get(), nullptr, nullptr, 0, nullptr, nullptr);
        fclose(fk);
    }

    FILE * fc = fopen(sc.data(), "wb");

    if (!fc)
    {
        LOG_ERROR("Failed to open cert file '{}' for writing", sc);
        return false;
    }

    PEM_write_X509(fc, cn.x509_.get());

    fclose(fc);

    return true;
}

bool save_cert_chain(const std::list< state > & chain)
{
    std::string dir = chain.back().cert_->common_name_ + "/";
    replace(dir, ' ', '_');

    _mkdir(dir.data());

    int save_key = chain.size();
    for (auto & s : chain)
    {
        --save_key;
        if (!save_cert_node(*s.cert_, dir, !save_key))
            return false;
    }

    return true;
}
