/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CERT_NODE_H
#define CERT_NODE_H

#include <string>
#include <openssl/x509.h>
#include <cassert>

struct cert_node
{
    std::unique_ptr< EVP_PKEY, decltype(&EVP_PKEY_free) > key_;
    std::unique_ptr< X509, decltype(&X509_free) > x509_;
    bool ca_;
    // ECC
    int curve_nid_;

    // Subject
    std::string country_;
    std::string state_;
    std::string locality_;
    std::string org_;
    std::string org_unit_;
    std::string common_name_;

    cert_node() :
        key_(nullptr, nullptr),
        x509_(nullptr, nullptr),
        ca_(false),
        curve_nid_(-1)
    { }
    cert_node(const cert_node & obj) = delete;
    cert_node(cert_node && obj) = default;
    cert_node & operator=(const cert_node & rhs) = delete;
    cert_node & operator=(cert_node && rhs) = default;

    void set_name(X509_NAME * n) const
    {
        assert(n != nullptr);

        X509_NAME_add_entry_by_txt(n, "C", MBSTRING_ASC, (unsigned char *)country_.c_str(), -1, -1, 0);
        X509_NAME_add_entry_by_txt(n, "ST", MBSTRING_ASC, (unsigned char *)state_.c_str(), -1, -1, 0);
        X509_NAME_add_entry_by_txt(n, "L", MBSTRING_ASC, (unsigned char *)locality_.c_str(), -1, -1, 0);
        X509_NAME_add_entry_by_txt(n, "O", MBSTRING_ASC, (unsigned char *)org_.c_str(), -1, -1, 0);
        X509_NAME_add_entry_by_txt(n, "OU", MBSTRING_ASC, (unsigned char *)org_unit_.c_str(), -1, -1, 0);
        X509_NAME_add_entry_by_txt(n, "CN", MBSTRING_ASC, (unsigned char *)common_name_.c_str(), -1, -1, 0);
    }
};

#endif
