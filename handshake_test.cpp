/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <filesystem>
#include <future>
#include <thread>
#include <string_view>
#include <sys/socket.h>
#include <sys/un.h>

#include "openssl/ssl.h"
#include "openssl/x509.h"

#include "program.h"
#include "Logger.h"
#include "arg_parser.h"

namespace fs = std::filesystem;

// TODO Function aliasing isn't a thing
template< fs::file_type FileType>
class path_exists
{
    public:
        path_exists(const std::string & path) :
            ret_(fs::exists(path) && fs::status(path).type() == FileType)
        { }

        operator bool() const { return ret_; }

    private:
        bool ret_;
};

using type_func = bool(*)(const std::filesystem::path &);
using directory_exists = path_exists< std::filesystem::file_type::directory >;
using file_exists = path_exists< std::filesystem::file_type::regular >;

class arg_parser : public unkn::arg_parser
{
    public:
        enum : int
        {
            SERVER_CERT = 0,
            SERVER_KEY,
            CLIENT_CERT,
            CLIENT_KEY,
            CA_PATH,
            EXPECT_SUCCESS
        };

        arg_parser()
        {
            add_opt< unkn::string_arg_opt >(SERVER_CERT, 'c', "server-cert", "Cert file");
            add_opt< unkn::string_arg_opt >(SERVER_KEY, 'k', "server-key", "Key file");
            add_opt< unkn::string_arg_opt >(CLIENT_CERT, 'C', "client-cert", "Cert file");
            add_opt< unkn::string_arg_opt >(CLIENT_KEY, 'K', "client-key", "Key file");
            add_opt< unkn::string_arg_opt >(CA_PATH, 'a', "ca-path", "CA Path");
            add_opt< unkn::bool_arg_opt >(EXPECT_SUCCESS, 's', "expect-success", "Expect a successful TLS handshake");
        }

        arg_parser(const arg_parser &) = delete;
        arg_parser(arg_parser &&) = default;
        virtual ~arg_parser() { }

        arg_parser & operator=(const arg_parser &) = delete;
        arg_parser & operator=(arg_parser &&) = default;

    protected:
        // TODO Validate should populate a structure with all the things we
        // want for ez access. From which point we can grab the struct and toss
        // the arg parser.
        bool validate() override
        {
            if (!unkn::arg_parser::validate())
                return false;

            return
                file_exists(get_opt(SERVER_CERT).value< const std::string & >()) &&
                file_exists(get_opt(SERVER_KEY).value< const std::string & >()) &&
                file_exists(get_opt(CLIENT_CERT).value< const std::string & >()) &&
                file_exists(get_opt(CLIENT_KEY).value< const std::string & >()) &&
                directory_exists(get_opt(CA_PATH).value< const std::string & >());
        }
};

template< typename TRet, typename TRead, TRead tread, typename TFree, TFree tfree>
inline typename std::unique_ptr< TRet, TFree > load_pem(const std::string & file)
{
    auto bio = std::unique_ptr< BIO, decltype(&BIO_free) >(
            BIO_new_file(file.c_str(), "r"), &BIO_free);
    if (!bio)
    {
        LOG_ERROR("Failed to open key file {}", file);
        return { nullptr, tfree };
    }
    return { tread(bio.get(), nullptr, nullptr, nullptr), tfree };
}

auto load_key = std::function(load_pem<
        EVP_PKEY,
        decltype(&PEM_read_bio_PrivateKey),
        &PEM_read_bio_PrivateKey,
        decltype(&EVP_PKEY_free),
        &EVP_PKEY_free>);

auto load_cert = std::function(load_pem<
        X509,
        decltype(&PEM_read_bio_X509_AUX),
        &PEM_read_bio_X509_AUX,
        decltype(&X509_free),
        &X509_free>);

auto load_dh = std::function(load_pem<
        DH,
        decltype(&PEM_read_bio_DHparams),
        &PEM_read_bio_DHparams,
        decltype(&DH_free),
        &DH_free>);

int verify_callback(int ok, X509_STORE_CTX *ctx)
{
    [[maybe_unused]] X509 * err_cert = X509_STORE_CTX_get_current_cert(ctx);
    [[maybe_unused]] int err = X509_STORE_CTX_get_error(ctx);
    [[maybe_unused]] int depth = X509_STORE_CTX_get_error_depth(ctx);

    // TODO print info
    // TODO verification stuff
    return ok;
}

static bool server_thread(int fd, const arg_parser & args, std::promise<void> && ready_for_client);
static bool client_thread(int fd, const arg_parser & args);
static SSL_CTX * new_SSL_CTX(bool server);
static bool load_stores(SSL_CTX * ctx, const std::string & chain_file);
static bool set_cert_and_key(SSL_CTX * ctx, X509 * cert, EVP_PKEY * key);

class program : public unkn::program< arg_parser >
{
    protected:
        int run() override
        {
            int fd[2];
            int & server_fd = fd[0];
            int & client_fd = fd[1];

            if (socketpair(AF_UNIX, SOCK_STREAM, 0, fd) != 0)
            {
                LOG_CRIT("Failed to create unix socket pair");
                return -1;
            }

            std::promise<void> p_server_ready;
            auto ready_for_client = p_server_ready.get_future();
            const auto & ap = this->arg_parser();

            auto a_server = std::async(std::launch::async, server_thread, server_fd, std::ref(ap), std::move(p_server_ready));
            ready_for_client.wait();
            auto a_client = std::async(std::launch::async, client_thread, client_fd, std::ref(ap));

            // TODO We need more than just true/false because there are
            // meaningful failures and fucked failures
            bool expect_success = this->arg_parser().get_opt(arg_parser::EXPECT_SUCCESS).value< bool >();

            return expect_success == (a_server.get() && a_client.get());
        }
};

int main(int argc, char ** argv)
{
    return program().main(argc, argv);
}

bool server_thread(
        int fd,
        const arg_parser & args,
        // TODO Signal when ready for client to initialize
        [[maybe_unused]] std::promise<void> && ready_for_client)
{
    SSL_CONF_CTX * cctx = SSL_CONF_CTX_new();
    assert(cctx);
    X509_VERIFY_PARAM * vpm = X509_VERIFY_PARAM_new();
    assert(vpm);

    const auto & chain_path =
        args.get_opt(arg_parser::CA_PATH).value< const std::string >();

    // TODO Investigate the flags
    // https://www.openssl.org/docs/manmaster/man3/SSL_CONF_CTX_set_flags.html
    SSL_CONF_CTX_set_flags(cctx,
            SSL_CONF_FLAG_CMDLINE |
            SSL_CONF_FLAG_SERVER);

    const auto & key_file = args.get_opt(arg_parser::SERVER_KEY).value< const std::string >();
    auto us_key = load_key(key_file.data());
    assert(us_key);

    const auto & cert_file = args.get_opt(arg_parser::SERVER_CERT).value< const std::string >();
    auto us_cert = load_cert(cert_file.data());
    assert(us_cert);

    // TODO 1650├>        if (s_chain_file != NULL) {
    // TODO 1687│     if (crl_file != NULL) {

    SSL_CTX * ctx = new_SSL_CTX(true);
    if (!ctx)
        return false;

    // TODO 1795├>        SSL_CTX_sess_set_cache_size(ctx, 128);

    if (!SSL_CTX_load_verify_locations(
                ctx,
                nullptr,
                chain_path.data()))
    {
        assert(!"Failed to load verify dir");
        return false;
    }

    // TODO 1845│     ssl_ctx_add_crls(ctx, crls, 0);
    SSL_CONF_CTX_set_ssl_ctx(cctx, ctx);
    // TODO Investigate the options from
    // https://www.openssl.org/docs/manmaster/man3/SSL_CONF_cmd.html
    SSL_CONF_CTX_finish(cctx);

    if (!load_stores(ctx, chain_path))
        return false;

    {
        // TODO this will fail for the current certs. Figure out
        // how to add the dh params to certs if there is any benefit
        //auto udh = load_dh(cert_file.data());
        //auto dh = udh.get();

        // This will use the highest preference curve for ECDH
        SSL_CTX_set_dh_auto(ctx, 1);
    }

    if (!set_cert_and_key(ctx, us_cert.get(), us_key.get()))
        return false;

    // TODO 1974│         && !SSL_CTX_use_serverinfo_file(ctx, s_serverinfo_file)) {

    SSL_CTX_set_verify(
            ctx,
            SSL_VERIFY_PEER |
            SSL_VERIFY_FAIL_IF_NO_PEER_CERT
            // TODO SSL_VERIFY_POST_HANDSHAKE
            // https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_verify.html
            ,
            verify_callback);

    // TODO 2031├>    if (!SSL_CTX_set_session_id_context(ctx,

    // TODO 2040│     SSL_CTX_set_cookie_generate_cb(ctx, generate_cookie_callback);
    // TODO 2041│     SSL_CTX_set_cookie_verify_cb(ctx, verify_cookie_callback);

    // TODO 2078│         SSL_CTX_set_client_CA_list(ctx, SSL_load_client_CA_file(CAfile));
    // I think this is the one we are looking for

    // TODO 1432│     SSL_CTX_set_keylog_callback(ctx, keylog_callback);

    SSL * con = SSL_new(ctx);

    // TODO 2223│         SSL_set_tlsext_debug_callback(con, tlsext_cb);
    // TODO 2224│         SSL_set_tlsext_debug_arg(con, bio_s_out);

    if (!SSL_clear(con))
    {
        LOG_ERROR("Error clearing tls connection");
        return false;
    }

    BIO * sbio = BIO_new_socket(fd, BIO_NOCLOSE);
    SSL_set_bio(con, sbio, sbio);
    SSL_set_accept_state(con);

    // TODO 2305│         BIO_set_callback(SSL_get_rbio(con), bio_dump_callback);
    // TODO 2306│         BIO_set_callback_arg(SSL_get_rbio(con), (char *)bio_s_out);
    // TODO 2311│             SSL_set_msg_callback(con, SSL_trace);
    // TODO 2319│         SSL_set_tlsext_debug_callback(con, tlsext_cb);
    // TODO 2320│         SSL_set_tlsext_debug_arg(con, bio_s_out);

    // With a blocking socket (whats being used at the moment),
    // SSL_accept will return once the handshake is complete
    int i = SSL_accept(con);

    if (i == 0)
    {
        LOG_ERROR("TLS Failure with clean exit");
        return false;
    }

    if (i < 0)
    {
        // TODO Need to handle this case if using non-blocking IO
        LOG_ERROR("Fatal TLS handshake failure");
        return false;
    }

    assert(i == 1);
    LOG_NOTICE("TLS handshake success");
    return true;
}

bool client_thread(
        int fd,
        const arg_parser & args)
{
    SSL_CONF_CTX * cctx = SSL_CONF_CTX_new();
    assert(cctx);
    X509_VERIFY_PARAM * vpm = X509_VERIFY_PARAM_new();
    assert(vpm);

    // TODO Investigate the flags
    // https://www.openssl.org/docs/manmaster/man3/SSL_CONF_CTX_set_flags.html
    SSL_CONF_CTX_set_flags(cctx,
            SSL_CONF_FLAG_CMDLINE |
            SSL_CONF_FLAG_CLIENT);

    const auto & chain_path =
        args.get_opt(arg_parser::CA_PATH).value< const std::string >();

    const auto & key_file = args.get_opt(arg_parser::CLIENT_KEY).value< const std::string & >();
    auto us_key = load_key(key_file.data());
    assert(us_key);

    const auto & cert_file = args.get_opt(arg_parser::CLIENT_CERT).value< const std::string & >();
    auto us_cert = load_cert(cert_file.data());
    assert(us_cert);

    // TODO 1631│     if (chain_file != NULL)
    // TODO 1637├>    if (crl_file != NULL)
    // TODO 1654├>    if (!load_excert(&exc))

    SSL_CTX * ctx = new_SSL_CTX(false);
    if (!ctx)
        return false;

    // TODO 1695├>    if (vpmtouched && !SSL_CTX_set1_param(ctx, vpm))
    // SSL_CTX_set_max_send_fragment
    // SSL_CTX_set_split_send_fragment
    // SSL_CTX_set_max_pipelines
    // SSL_CTX_set_default_read_buffer_len
    // SSL_CTX_set_tlsext_max_fragment_length
    // 1165│         if (SSL_CONF_cmd(cctx, flag, arg) <= 0)
    // 1175│     if (!SSL_CONF_CTX_finish(cctx))

    SSL_CONF_CTX_set_ssl_ctx(cctx, ctx);
    // TODO Investigate the options from
    // https://www.openssl.org/docs/manmaster/man3/SSL_CONF_cmd.html
    SSL_CONF_CTX_finish(cctx);

    if (!load_stores(ctx, chain_path))
        return false;

    // TODO 1756│         SSL_CTX_set0_CA_list(ctx, nm);
    // TODO 1760│         if (!SSL_CTX_set_client_cert_engine(ctx, ssl_client_engine))
    // may need this for random smart card things
    // TODO 1808│         ssl_ctx_set_excert(ctx, exc);
    // TODO 1831│         if (!SSL_CTX_add_client_custom_ext(ctx,
    // TODO 1842│         SSL_CTX_set_info_callback(ctx, apps_ssl_info_callback);

    // TODO Do we have a use case for certificate transparancy? probably
    // 1847│         !SSL_CTX_enable_ct(ctx, SSL_CT_VALIDATION_PERMISSIVE)) {
    // 1852├>    if (!ctx_set_ctlog_list_file(ctx, ctlog_file)) {
    // the sct stuff looks interesting
    // https://www.globalsign.com/en/blog/what-is-certificate-transparency/

    SSL_CTX_set_verify(
            ctx,
            // TODO Don't use anonymous ciphers becuase peer
            // verification is ignored if the server doesn't send a
            // cert
            SSL_VERIFY_PEER,
            verify_callback);

    // NOTE Order is diff between client and server; doesnt seem important
    if (!SSL_CTX_load_verify_locations(
                ctx,
                nullptr,
                chain_path.data()))
    {
        assert(!"Failed to load verify dir");
        return false;
    }

    // TODO 1875├>    ssl_ctx_add_crls(ctx, crls, crl_download);

    if (!set_cert_and_key(ctx, us_cert.get(), us_key.get()))
        return false;

    // TODO 1882│         SSL_CTX_set_tlsext_servername_callback(ctx, ssl_servername_cb);
    // TODO 1883│         SSL_CTX_set_tlsext_servername_arg(ctx, &tlsextcbp);

    SSL * con = SSL_new(ctx);
    assert(con);

    // TODO 1931│         SSL_force_post_handshake_auth(con);
    // TODO 2092│         SSL_set_tlsext_debug_callback(con, tlsext_cb);
    // TODO 2093│         SSL_set_tlsext_debug_arg(con, bio_c_out);
    // TODO 2097│         SSL_set_tlsext_status_type(con, TLSEXT_STATUSTYPE_ocsp);
    // TODO 2098│         SSL_CTX_set_tlsext_status_cb(ctx, ocsp_resp_cb);
    // TODO 2099│         SSL_CTX_set_tlsext_status_arg(ctx, bio_c_out);

    BIO * sbio = BIO_new_socket(fd, BIO_NOCLOSE);
    // Not sure why we do this yet... has to do with read bio being the
    // write bio
    // TODO It didn't look like we do it for the server tho?
    BIO_up_ref(sbio);
    SSL_set0_rbio(con, sbio);
    SSL_set0_wbio(con, sbio);

    SSL_set_connect_state(con);

    std::string_view s = "pineapples";
    int i = SSL_write(con, s.data(), s.size() + 1);
    int err = SSL_get_error(con, i);

    switch (err)
    {
        case SSL_ERROR_NONE:
            LOG_ERROR("Client error none unhandled");
            break;

        case SSL_ERROR_WANT_WRITE:
            LOG_ERROR("Client error want write unhandled");
            break;

        case SSL_ERROR_WANT_READ:
            LOG_ERROR("Client error want read unhandled");
            break;

        case SSL_ERROR_SYSCALL:
            LOG_ERROR("Client error syscall unhandled");
            break;

        default:
            LOG_ERROR("Unhandled client send error: {}", err);
            break;
    }

    return true;
}

SSL_CTX * new_SSL_CTX(bool server)
{
    auto method = server ? TLS_server_method() : TLS_client_method();
    SSL_CTX * ctx = SSL_CTX_new(method);
    assert(ctx);

    // TODO 1682│         if (SSL_CTX_config(ctx, ssl_config) == 0)


    if (!SSL_CTX_set_min_proto_version(ctx, TLS1_3_VERSION) ||
            !SSL_CTX_set_max_proto_version(ctx, TLS1_3_VERSION))
    {
        assert(!"Failed to force TLS 1.3");
        SSL_CTX_free(ctx);
        return nullptr;
    }

    return ctx;
}

bool load_stores(SSL_CTX * ctx, const std::string & chain_path)
{
    {
        auto uvfy = std::unique_ptr< X509_STORE, decltype(&X509_STORE_free) >(
                X509_STORE_new(), &X509_STORE_free);
        auto vfy = uvfy.get();
        assert(vfy);
        // TODO is this redundant?
        if (!X509_STORE_load_locations(vfy, nullptr, chain_path.data()))
        {
            assert(!"Failed to load x509 verify dir");
            return false;
        }
        // TODO 1217│         add_crls_store(vfy, crls);
        SSL_CTX_set1_verify_cert_store(ctx, vfy);
    }

    {
        auto uch = std::unique_ptr< X509_STORE, decltype(&X509_STORE_free) >(
                X509_STORE_new(), &X509_STORE_free);
        auto ch = uch.get();
        assert(ch);
        if (!X509_STORE_load_locations(ch, nullptr, chain_path.data()))
        {
            assert(!"Failed to chain dir");
            return false;
        }
        // TODO 1217│         add_crls_store(vfy, crls);
        SSL_CTX_set1_chain_cert_store(ctx, ch);
    }

    return true;
}

bool set_cert_and_key(SSL_CTX * ctx, X509 * cert, EVP_PKEY * key)
{
    assert(ctx);
    assert(cert);
    assert(key);

    if (!SSL_CTX_use_certificate(ctx, cert))
    {
        assert(!"Failed to use certificate");
        return false;
    }

    if (!SSL_CTX_use_PrivateKey(ctx, key))
    {
        assert(!"Failed to use private key");
        return false;
    }

    if (!SSL_CTX_check_private_key(ctx))
    {
        assert(!"Failed to check private key");
        return false;
    }

    // TODO  169│     if (chain && !SSL_CTX_set1_chain(ctx, chain)) {
    // TODO  174│     if (build_chain && !SSL_CTX_build_cert_chain(ctx, chflags)) {

    return true;
}
