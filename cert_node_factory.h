/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CERT_NODE_FACTORY_H
#define CERT_NODE_FACTORY_H

#include <functional>
#include <cassert>

#include "Logger.h"
#include "tao/json.hpp"
#include "cert_node.h"
#include "json_utils.h"

class cert_node_factory
{
    public:
        cert_node_factory(const cert_node & def);
        virtual ~cert_node_factory() {}

        std::unique_ptr<cert_node> create(const tao::json::value & v);

    private:
        const cert_node & default_;

        void set_defaults(cert_node & cn) const;

        bool get_string(
                const std::string & key,
                const std::map<std::string, tao::json::value> & o,
                const std::string & def,
                std::string & out);

        bool get_string(
                const std::string & key,
                const std::map<std::string,
                tao::json::value> & o,
                std::string & out);

        bool get_curve(const std::map<std::string, tao::json::value> & o, int & out);
        bool get_bool(const std::string & key, const std::map<std::string, tao::json::value> & o, bool def, bool & out);
};

#endif
