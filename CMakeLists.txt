cmake_minimum_required(VERSION 3.9.1)
project(test-cert-tree-gen)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")

find_package(OpenSSL REQUIRED)
find_package(Threads REQUIRED)

set(USE_UNKN_LIBS "program")
set(TAOCPP_JSON_BUILD_TESTS OFF)
set(TAOCPP_JSON_BUILD_EXAMPLES OFF)

add_subdirectory(json)
add_subdirectory(libunkn)

add_executable(${CMAKE_PROJECT_NAME}
    main.cpp
    tree_gen_arg_parser.cpp
    json_utils.cpp
    cert_node_factory.cpp
    )
target_link_libraries(${CMAKE_PROJECT_NAME}
    program
    ${OPENSSL_LIBRARIES}
    taocpp-json
    )
set_property(TARGET ${CMAKE_PROJECT_NAME} PROPERTY CXX_STANDARD 17)

add_executable(handshake_test handshake_test.cpp common.cpp)
target_link_libraries(handshake_test
    program
    logger
    ${OPENSSL_LIBRARIES}
    Threads::Threads
    stdc++fs)
set_property(TARGET handshake_test PROPERTY CXX_STANDARD 17)
