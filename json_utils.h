/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef JSON_UTILS_H
#define JSON_UTILS_H

#include "tao/json.hpp"

const char * json_type_to_str(tao::json::type t);

#endif
