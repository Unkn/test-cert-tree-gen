/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <openssl/objects.h>
#include <openssl/ec.h>

#include "cert_node_factory.h"

cert_node_factory::cert_node_factory(const cert_node & def) :
    default_(def)
{
}

std::unique_ptr<cert_node> cert_node_factory::create(const tao::json::value & v)
{
    using namespace tao::json;

    if (v.type() != type::OBJECT)
    {
        LOG_ERROR("Expected node of type {} but got {}", json_type_to_str(type::OBJECT), json_type_to_str(v.type()));
        return nullptr;
    }

    auto & o = v.unsafe_get_object();
    auto ret = std::make_unique<cert_node>();
    set_defaults(*ret);

    if (!get_string("CN", o, ret->common_name_) ||
            !get_curve(o, ret->curve_nid_) ||
            !get_string("C", o, default_.country_, ret->country_) ||
            !get_string("ST", o, default_.state_, ret->state_) ||
            !get_string("L", o, default_.locality_, ret->locality_) ||
            !get_string("O", o, default_.org_, ret->org_) ||
            !get_string("OU", o, default_.org_unit_, ret->org_unit_) ||
            !get_bool("ca", o, false, ret->ca_))
    {
        return nullptr;
    }

    return ret;
}

void cert_node_factory::set_defaults(cert_node & cn) const
{
    cn.curve_nid_ = default_.curve_nid_;
    cn.country_ = default_.country_;
    cn.state_ = default_.state_;
    cn.locality_ = default_.locality_;
    cn.org_ = default_.org_;
    cn.org_unit_ = default_.org_unit_;
}

bool cert_node_factory::get_string(
        const std::string & key,
        const std::map<std::string, tao::json::value> & o,
        const std::string & def,
        std::string & out)
{
    using namespace tao::json;

    bool required = def.empty();
    auto it = o.find(key);
    if (it == o.end())
    {
        if (required)
        {
            LOG_ERROR("Missing '{}' field", key);
            return false;
        }

        assert(!def.empty());

        LOG_INFO("Set '{}' field to default '{}'", key, def);
        out = def;
        return true;
    }

    if (it->second.type() != type::STRING)
    {
        LOG_INFO("Expected value type {} for field '{}' but got {}",
                json_type_to_str(type::STRING),
                key,
                json_type_to_str(it->second.type()));
        return false;
    }

    out = it->second.unsafe_get_string();
    if (out.empty())
    {
        LOG_ERROR("Recieved empty string for field '{}'", key);
        return false;
    }

    return true;
}

bool cert_node_factory::get_string(
        const std::string & key,
        const std::map<std::string, tao::json::value> & o,
        std::string & out)
{
    return get_string(key, o, "", out);
}

bool cert_node_factory::get_curve(const std::map<std::string, tao::json::value> & o, int & out)
{
    std::string curve_name;
    // TODO The default curve should be command line configurable by the NIST name
    bool req = default_.curve_nid_ == -1;
    const char * def = req ? "" : EC_curve_nid2nist(default_.curve_nid_);

    // If we have a default curve but EC_curve_nid2nist does not find the name,
    // then it must not be a valid default
    assert(def);

    if (!get_string("curve_name", o, def, curve_name))
    {
        return false;
    }

    int nid = EC_curve_nist2nid(curve_name.c_str());
    if (nid == NID_undef)
    {
        LOG_ERROR("Identifier `{}` is not a valid NIST curve", curve_name);
        return false;
    }

    out = nid;
    return true;
}

bool cert_node_factory::get_bool(
        const std::string & key,
        const std::map<std::string, tao::json::value> & o,
        bool def,
        bool & out)
{
    using namespace tao::json;

    auto it = o.find(key);
    if (it == o.end())
    {
        LOG_INFO("Set '{}' field to default '{}'", key, def);
        out = def;
        return true;
    }

    if (it->second.type() != type::BOOLEAN)
    {
        LOG_INFO("Expected value type {} for field '{}' but got {}",
                json_type_to_str(type::BOOLEAN),
                key,
                json_type_to_str(it->second.type()));
        return false;
    }

    out = it->second.unsafe_get_boolean();

    return true;
}
