/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cassert>

#include "json_utils.h"
#include "Logger.h"

const char * json_type_to_str(tao::json::type t)
{
    using namespace tao::json;

    switch (t)
    {
        case type::UNINITIALIZED: return "UNINITIALIZED";
        case type::DISCARDED: return "DISCARDED";
        case type::DESTROYED: return "DESTROYED";
        case type::NULL_: return "NULL_";
        case type::BOOLEAN: return "BOOLEAN";
        case type::SIGNED: return "SIGNED";
        case type::UNSIGNED: return "UNSIGNED";
        case type::DOUBLE: return "DOUBLE";
        case type::STRING: return "STRING";
        case type::STRING_VIEW: return "STRING_VIEW";
        case type::BINARY: return "BINARY";
        case type::BINARY_VIEW: return "BINARY_VIEW";
        case type::ARRAY: return "ARRAY";
        case type::OBJECT: return "OBJECT";
        case type::RAW_PTR: return "RAW_PTR";
        case type::OPAQUE_PTR: return "OPAQUE_PTR";
    }

    LOG_ERROR("Invalid json type: {}", static_cast<uint8_t>(t));
    assert(!"Invalid json type");
    return "Unknown";
}
